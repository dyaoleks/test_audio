
from flask import Flask, render_template, request, jsonify
import torch
from transformers import AutoModelForSpeechSeq2Seq, AutoProcessor, pipeline
import logging
from flask_sqlalchemy import SQLAlchemy
import os
import psycopg2
from dotenv import load_dotenv
import sys

load_dotenv()

app = Flask(__name__, template_folder="templates")
# Зчитуємо URL підключення до бази даних з .env
url = os.getenv('SQLALCHEMY_DATABASE_URL')

# Виводимо URL підключення до бази даних
print("URL підключення до бази даних:", url)

# Підключення до бази даних через SQLAlchemy (розкоментуйте, якщо плануєте використовувати SQLAlchemy)
# app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI')
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# db = SQLAlchemy(app)

# Ініціалізуємо логер з виведенням в файл і на консоль
logging.basicConfig(filename='error.log', level=logging.ERROR)
logging.basicConfig(stream=sys.stdout, level=logging.ERROR)

device = "cuda:0" if torch.cuda.is_available() else "cpu"
torch_dtype = torch.float16 if torch.cuda.is_available() else torch.float32
model_id = "openai/whisper-large-v3"

model = AutoModelForSpeechSeq2Seq.from_pretrained(
    model_id, torch_dtype=torch_dtype, use_safetensors=True
)
model.to(device)

processor = AutoProcessor.from_pretrained(model_id)

pipe = pipeline(
    "automatic-speech-recognition",
    model=model,
    tokenizer=processor.tokenizer,
    feature_extractor=processor.feature_extractor,
    max_new_tokens=128,
    chunk_length_s=30,
    batch_size=16,
    return_timestamps=True,
    torch_dtype=torch_dtype,
    device=device,
)

# Створення таблиці у базі даних (розкоментуйте, якщо використовуєте SQLAlchemy)
# with app.app_context():
#     db.create_all()

@app.route('/', methods=['GET', 'POST'])
def index():
    transcription = None

    if request.method == 'POST':
        if 'audio' not in request.files or not request.files['audio']:
            return jsonify({'error': 'No audio file provided or file is empty'}), 400

        audio_file = request.files['audio']
        audio_bytes = audio_file.read()

        try:
            result = pipe(audio_bytes)
            transcription = result['text']
        except Exception as e:
            # Логування помилки
            logging.error(f"An error occurred while processing audio: {str(e)}")
            return jsonify({'error': str(e)}), 500

    return render_template('index.html', transcription=transcription)

@app.route('/transcribe', methods=['POST'])
def transcribe():
    if 'audio' not in request.files or not request.files['audio']:
        return jsonify({'error': 'No audio file provided or file is empty'}), 400

    audio_file = request.files['audio']
    audio_bytes = audio_file.read()

    try:
        result = pipe(audio_bytes)
        transcription = result['text']
    except Exception as e:
        # Логування помилки
        logging.error(f"An error occurred while transcribing audio: {str(e)}")
        return jsonify({'error': str(e)}), 500

    return jsonify({'transcription': transcription})

if __name__ == '__main__':
    app.run(debug=True, use_reloader=False, host="0.0.0.0")